# coding=UTF-8

from flask import Flask
from flask import render_template
from flask import request
import re

import sys
if sys.version_info[0] < 3:
    from  urllib import quote
else:
    from urllib.parse import quote


app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    AURL = ''
    output = ''
    if request.method=='POST': 
        output = quote(request.values["AURL"])
        AURL = request.values["AURL"]
    with open("templates/index.html", 'rb') as f:
        content = f.read()
        resp = re.sub('\$OUTPUT', output, content.decode('utf-8'))
        resp = re.sub('\$AURL', AURL, resp)
    return resp


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=34567)
